
/*	
	Define the mySisenseApp object, which will contain 
	all properties/methods for interacting with sisense.js
*/

var mySisenseApp = {
	settings: {
		server: "http://takashi.sisense.com",
		elasticube: {
			title: "Sample Healthcare", 
			id: "aLOCALHOST_aSAMPLEIAAaHEALTHCARE", 
			address: "localHost", 
			database: "aSampleIAAaHealthcare"
		}
	},
	app: null,
	dashboard: null,
	filters: {}
}