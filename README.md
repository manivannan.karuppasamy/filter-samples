# Sisense.js Custom Filter Samples

This project was created to show several ways to integrate 3rd party components with Sisense.js.  Many web apps that leverage Sisense.js for embedding do so because they already have a consistent look/feel for their application and want the controls related to Sisense to match.  This sample project showcases a few common filter types, in a simple web page.  This webpage uses [Semantic UI](https://semantic-ui.com/introduction/getting-started.html) framework, but this approach would be similar for Bootstrap, Material Design, etc 

## Workflow
Almost all custom filter controls follow the same workflow: 

* Fetch data - Make an API call to fetch data from Sisense, and add the results as potential selections in the UI
* Initialization - Setup the component with this data
* User Selection - When a user makes a selection on this control, set a filter on your Sisense dashboard using our JavaScript API

## Filter Samples

### Toggle
A simple toggle switch, that sets/unsets a filter when clicked

![Toggle Switch](screenshots/toggle.png)

### Dropdown
A dropdown menu built using Semantic UI's [dropdown](https://semantic-ui.com/modules/dropdown.html) framework.  This example has a simple dropdown which fetches a list of unique values from Sisense, and sets a filter based on user selection (supports multi-select).  

![Simple Dropdown](screenshots/dropdown-simple.png)

Also included is a more complex dropdown that nests multiple dimensions into levels.  This takes a bit of HTML manipulation to create the hierarchy, but generally it works the same as the simple example.

![Advanced Dropdown](screenshots/dropdown-advanced.png)

### Calendar Date Range Picker
A dropdown that displays a date range picker, based on an open source [JavaScript component](http://www.daterangepicker.com/).  This does require adding moment.js, daterangepicker.js, and daterangepicker.css into your HTML page, but the JavaScript implementation of this component is similar to the rest of the examples.  One of the nice features of this component, is that it allows you to define shortcuts for  date ranges (yesterday, last month, etc)

![Calendar Picker](screenshots/date-range.png)

### Reset Button
This is a simple button that resets the filters on the page.  Resetting the filters for your Sisense dashboard is pretty simple through the JavaScript API, most of the code here is for resetting the UI for the other filter components.

![Toggle Switch](screenshots/reset-button.png)

## File Structure

### semantic
This directory contains all files required for Semantic UI.  No customizations were made here

### config.js
This file contains the base object that will store a reference to all settings/methods related to this website.  When setting this up, you should update the server address and potentially the Elasticube.  

### index.html
This HTML page is the default landing page for this website.  It loads *config.js* and *main.js*, to initialize the application, and includes the definition of the filter components and widgets to display.  This sample uses widgets from the *Sample Healthcare* dashboard that comes with the free trial of Sisense, so if you change the Elasticube in config.js you should also change the widget IDs and filter properties defined in this file.

### main.css
This contains any styling outside of the Semantic UI framework

### main.js
This is the main JavaScript file that initializes the web page.  This file is split into two sections: Initialization & Runtime

#### Initialization

1. initScript - Adds a <script /> tag to your HTML page, to add the Sisense.js Library

2. connect - Once the Sisense.js library has finished loading, connect to your Sisense server

3. loadApplication - Once connected, create a new Dashboard and find all the widgets & filters on the web page.  For each widget found, run the *loadWidget* function.  For each filter found, run the *initFilter* function.

4. loadWidget - For a given widget ID, load the widget and render to the page

5. initFilter - For a given filter, initialize it and add an event handler for when a user interacts with it

#### Runtime

1. setFilter - Runs after a filter selection was made, sets the filter using Sisense JavaScript API

2. initScript() - Kicks off the initialization workflow

## Further help

To get more help on Semantic UI their [Documentation](https://semantic-ui.com/introduction/getting-started.html).

For more help with Sisense.js, check out the Sisense documentation [here](https://developer.sisense.com/display/API2/SisenseJS).